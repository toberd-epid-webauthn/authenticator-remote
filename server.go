// Copyright 2021 Tobias Berdin

package main

import (
	"authenticator-remote/config"
	"authenticator-remote/core"
	"authenticator-remote/request"
	"fmt"
	"net/http"
)

func main() {
	// initialize sessions
	core.InitSessions()

	http.HandleFunc("/", request.Hello)
	http.HandleFunc("/session", request.Session)
	http.HandleFunc("/create", request.Create)
	http.HandleFunc("/authenticate", request.Authenticate)

	port := config.Port

	fmt.Printf("Listening on port %s ...\n", port)
	// start server on the specified port
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println("Finished")
}
