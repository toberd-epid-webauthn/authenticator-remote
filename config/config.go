// Copyright 2022 Tobias Berdin

// Package config contains all configuration parameters.
package config

var (
	// Port specifies the port on which the API runs.
	Port = "8080"

	// EPIDVerifier specifies where the EPID verifier is located.
	// Path must not end with a '/'.
	EPIDVerifier = "https://your.epid.verifier"

	// Secret specifies the secret parameter for credential creation. Should be
	// randomly set.
	// CHANGEME!
	Secret = "0123456789abcdef[=!=.^xV98[q6[,\")o2a~Pj[B=C\"<CX@kgQmp;qJ[}"
)
