module authenticator-remote

go 1.16

require (
	github.com/fxamacker/cbor/v2 v2.4.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)
