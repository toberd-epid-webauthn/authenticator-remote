// Copyright 2022 Tobias Berdin

package util

import (
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rsa"
	"crypto/sha256"
	"golang.org/x/crypto/hkdf"
)

// GenerateRSAKey generates an RSA keypair based on the given algorithm.
//
// COSE -257, -258, -259
func GenerateRSAKey(secret []byte, alg string) (*rsa.PrivateKey, *rsa.PublicKey) {
	hash := sha256.New
	generator := hkdf.New(hash, secret, nil, nil)
	switch alg {
	case "-257": // RS256
		key, _ := rsa.GenerateKey(generator, 256)
		return key, &key.PublicKey
	case "-258": // RS384
		key, _ := rsa.GenerateKey(generator, 384)
		return key, &key.PublicKey
	case "-259": // RS512
		key, _ := rsa.GenerateKey(generator, 512)
		return key, &key.PublicKey
	default:
		return nil, nil
	}
}

// GenerateECDSAKey generates an ECDSA keypair.
//
// COSE -7, -35, 36
func GenerateECDSAKey(secret []byte, alg string) (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	hash := sha256.New
	generator := hkdf.New(hash, secret, nil, nil)
	switch alg {
	case "-7": // ES256
		key, _ := ecdsa.GenerateKey(elliptic.P256(), generator)
		return key, &key.PublicKey
	case "-35": // ES384
		key, _ := ecdsa.GenerateKey(elliptic.P384(), generator)
		return key, &key.PublicKey
	case "-36": // ES521
		key, _ := ecdsa.GenerateKey(elliptic.P521(), generator)
		return key, &key.PublicKey
	default:
		return nil, nil
	}
}

// GenerateEdDSA generates an EdDSA keypair.
//
// COSE -8
func GenerateEdDSA(secret []byte) (*ed25519.PrivateKey, *ed25519.PublicKey) {
	hash := sha256.New
	generator := hkdf.New(hash, secret, nil, nil)
	publicKey, privateKey, _ := ed25519.GenerateKey(generator)
	return &privateKey, &publicKey
}

// Hash produces a sha256 hash from the given string.
func Hash(s string) []byte {
	hash := sha256.New()
	hash.Write([]byte(s))

	hashValue := hash.Sum(nil)

	return hashValue
}
