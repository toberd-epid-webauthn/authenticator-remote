// Copyright 2022 Tobias Berdin

package util

import "github.com/fxamacker/cbor/v2"

var ctap2mode, _ = cbor.CTAP2EncOptions().EncMode()

// Marshal cbor encodes the given interface v.
func Marshal(v interface{}) ([]byte, error) {
	return ctap2mode.Marshal(v)
}
