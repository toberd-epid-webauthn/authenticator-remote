// Copyright 2022 Tobias Berdin

package util

import "encoding/binary"

// ToIntArray translates a byte array into an array of integers.
func ToIntArray(b []byte) []int {
	ret := make([]int, len(b))
	for i, v := range b {
		result := int(v)
		ret[i] = result
	}
	return ret
}

// Uint16ToByte transforms a uint32 to byte.
func Uint16ToByte(val uint16) []byte {
	b := make([]byte, 2)
	binary.BigEndian.PutUint16(b, val)
	return b
}

// Uint32ToByte transforms a uint32 to byte.
func Uint32ToByte(val uint32) []byte {
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, val)
	return b
}

// Uint64ToByte transforms a uint64 to byte.
func Uint64ToByte(val uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, val)
	return b
}
