// Copyright 2022 Tobias Berdin

package request

import (
	"authenticator-remote/config"
	"authenticator-remote/core"
	"authenticator-remote/util"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func Authenticate(w http.ResponseWriter, r *http.Request) {
	// add cors header to all responses
	w.Header().Add("Access-Control-Allow-Origin", "*")

	// ensure that request is POST
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// read request body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println("error while reading request: ", err.Error())
		http.Error(w, "Internal server error: error while reading request", http.StatusInternalServerError)
		return
	}

	// close request body reader
	err = r.Body.Close()
	if err != nil {
		fmt.Println("error while closing request reader: ", err.Error())
		http.Error(w, "Internal server error: error while closing request body", http.StatusInternalServerError)
		return
	}

	if len(body) == 0 {
		http.Error(w, "Bad request: empty body", http.StatusBadRequest)
		return
	}

	// parse request body into core.AuthenticationRequest
	request := core.AuthenticateRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		fmt.Println("error during json unmarshal: ", err.Error())
		http.Error(w, "Internal server error: error while reading request body", http.StatusInternalServerError)
		return
	}

	// get challenge from session id
	challenge := core.GetSession(request.Session)
	if challenge == nil {
		http.Error(w, "Bad request: session does not exist", http.StatusBadRequest)
		return
	}

	// create verifysig request
	signature := request.Signature
	postBody, _ := json.Marshal(map[string]interface{}{
		"signature": signature,
		"message":   challenge,
		"publicKey": util.ToIntArray(request.EPIDPublicKey),
	})
	byteBody := bytes.NewBuffer(postBody)

	// send verifysig request
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	resp, err := http.Post(config.EPIDVerifier+"/verifysig", "application/json", byteBody)
	if err != nil {
		fmt.Println("error during verifysig request: ", err.Error())
		http.Error(w, "Internal server error: error while verifying signature", http.StatusInternalServerError)
		return
	}

	// check if signature was verified
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("Session %s not verified\n", request.Session)
		http.Error(w, "Bad request: signature couldn't be verified", http.StatusBadRequest)
		return
	}

	// recreate credentials from the authenticate request
	credentials, err := request.Recreate()
	if err != nil {
		fmt.Println("error while creating credentials: ", err.Error())
		http.Error(w, "Internal server error: error while creating credentials", http.StatusInternalServerError)
		return
	}

	// parse response
	response, err := json.Marshal(credentials)
	if err != nil {
		fmt.Println("error during json marshal: ", err.Error())
		http.Error(w, "Internal server error: error while creating response body", http.StatusInternalServerError)
		return
	}

	fmt.Printf("Session %s verified\n", request.Session)

	// send response to client
	_, err = fmt.Fprint(w, string(response))
	if err != nil {
		fmt.Println("error while sending response: ", err.Error())
		http.Error(w, "Internal server error: error while sending response", http.StatusInternalServerError)
	}
}
