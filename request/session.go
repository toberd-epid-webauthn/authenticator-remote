// Copyright 2022 Tobias Berdin

package request

import (
	"authenticator-remote/core"
	"authenticator-remote/util"
	"encoding/json"
	"fmt"
	"net/http"
)

func Session(w http.ResponseWriter, r *http.Request) {
	// add cors header to all responses
	w.Header().Add("Access-Control-Allow-Origin", "*")

	// ensure that request is POST
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// generate a 32 byte random challenge
	challenge := util.RandString(32)

	// generate a 32 byte random sessionID
	sessionID := util.RandString(32)

	// create response and convert it to JSON
	responseStruct := core.ChallengeResponse{Challenge: challenge, Session: sessionID}
	response, err := json.Marshal(responseStruct)
	if err != nil {
		fmt.Println("error during json marshal: ", err.Error())
		http.Error(w, "Internal server error: error while creating response body", http.StatusInternalServerError)
		return
	}

	// store challenge
	core.AddSession(sessionID, challenge)
	fmt.Printf("Session %s initialized\n", sessionID)

	// send response to client
	_, err = fmt.Fprint(w, string(response))
	if err != nil {
		fmt.Println("error while sending response: ", err.Error())
		http.Error(w, "Internal server error: error while sending response", http.StatusInternalServerError)
	}
}
