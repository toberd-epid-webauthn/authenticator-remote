// Copyright 2022 Tobias Berdin

package core

import (
	"authenticator-remote/config"
	"authenticator-remote/util"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/fxamacker/cbor/v2"
	"strings"
)

// global aaguid
var aaguid = []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}

// global count for all signatures
var signCount = uint32(0)

// Create creates the public key credentials based on the creation request's
// EPIDPublicKey and the ID of the relying party.
func (c *CreateRequest) Create() (*PublicKeyCredential, error) {
	epidPublicKey := c.EPIDPublicKey
	rpID := c.PublicKey.Rp.Id

	// create seed based on the epidpublickey, the rp id and the secret
	seed := string(epidPublicKey) + rpID + config.Secret

	// create keypair based on the seed
	// we always create ecdsa P-256 keys as standard
	_, pk := util.GenerateECDSAKey([]byte(seed), "-7")

	// create a credential id based on the epidpublickey and the rp id
	credID := util.Hash(string(epidPublicKey) + rpID)

	origin := rpID
	if !strings.HasPrefix(rpID, "https://") {
		origin = "https://" + rpID
	}

	// assemble client data
	challengeBase64 := base64.RawURLEncoding.EncodeToString(c.PublicKey.Challenge)
	clientData := CollectedClientData{
		Type:        "webauthn.create",
		Challenge:   challengeBase64,
		Origin:      origin,
		CrossOrigin: false,
	}
	clientDataJSON, err := json.Marshal(clientData)
	if err != nil {
		return nil, errors.New("error while assembling client data json: " + err.Error())
	}

	// assemble cose public key
	credPubKey := CredentialPublicKeyES256{
		Kty: uint8(2),
		Alg: int8(-7),
		Crv: uint8(1),
		X:   pk.X.Bytes(),
		Y:   pk.Y.Bytes(),
	}
	credPubKeyCBOR, err := util.Marshal(credPubKey)
	if err != nil {
		return nil, errors.New("error while assembling credential public key: " + err.Error())
	}

	// assemble attested credential data
	attestedCredentialData := AttestedCredentialData{
		CredentialID:        string(credID),
		CredentialIDLength:  uint16(len(credID)),
		CredentialPublicKey: credPubKeyCBOR,
	}

	// assemble authenticator data
	signCount++
	authData := AuthenticatorData{
		RpIDHash:               util.Hash(rpID),
		Flags:                  byte(69),
		SignCount:              signCount,
		AttestedCredentialData: attestedCredentialData,
	}

	// assemble attestation object
	attestationObject := AttestationObject{
		AttStmt:  cbor.RawMessage{},
		Fmt:      "none",
		AuthData: authData.ToByteArray(),
	}
	marshal, err := util.Marshal(attestationObject)
	if err != nil {
		return nil, errors.New("error while assembling attestation object: " + err.Error())
	}

	response := AuthenticatorAttestationResponse{
		ClientDataJSON:    util.ToIntArray(clientDataJSON),
		AttestationObject: util.ToIntArray(marshal),
	}
	credIDBase64 := base64.RawURLEncoding.EncodeToString(credID)
	credentials := &PublicKeyCredential{
		ID:       credIDBase64,
		RawId:    util.ToIntArray(credID),
		Response: &response,
		Type:     "public-key",
	}
	return credentials, nil
}
