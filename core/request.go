// Copyright 2021 Tobias Berdin

package core

// CreateRequest is used when the client wants to create new credentials.
// Verifies the signature from the previously received challenge.
type CreateRequest struct {
	EPIDPublicKey []byte                             `json:"EPIDPublicKey"`
	Session       string                             `json:"session"`
	Signature     []int                              `json:"signature"`
	PublicKey     PublicKeyCredentialCreationOptions `json:"publicKey"`
}

// AuthenticateRequest is used when the client wants to recreate credentials
// in order to authenticate. Verifies the signature from the previously
// received challenge.
type AuthenticateRequest struct {
	EPIDPublicKey []byte                            `json:"EPIDPublicKey"`
	Session       string                            `json:"session"`
	Signature     []int                             `json:"signature"`
	PublicKey     PublicKeyCredentialRequestOptions `json:"publicKey"`
}
