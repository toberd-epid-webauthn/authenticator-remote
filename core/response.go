// Copyright 2021 Tobias Berdin

package core

// PublicKeyCredential contains the attributes that are returned to the caller
// when a new credential is created.
//
// see https://www.w3.org/TR/webauthn-3/#iface-pkcredential
type PublicKeyCredential struct {
	ID       string                `json:"id"`
	RawId    []int                 `json:"rawId"`
	Response AuthenticatorResponse `json:"response"`
	Type     string                `json:"type"`
}

// AuthenticatorResponse contains the objects being returned to the Relying
// Party.
//
// see https://www.w3.org/TR/webauthn-3/#iface-authenticatorresponse
type AuthenticatorResponse interface {
	GetClientDataJSON() []int
}

// AuthenticatorAttestationResponse represents the authenticator's response to
// a client’s request for the creation of a new public key credential.
//
// see https://www.w3.org/TR/webauthn-3/#iface-authenticatorattestationresponse
type AuthenticatorAttestationResponse struct {
	ClientDataJSON    []int `json:"clientDataJSON"`
	AttestationObject []int `json:"attestationObject"`
}

// AuthenticatorAssertionResponse represents an authenticator's response to a
// client’s request for generation of a new authentication assertion given the
// Relying Party's challenge and optional list of credentials it is aware of.
//
// see https://www.w3.org/TR/webauthn-3/#iface-authenticatorassertionresponse
type AuthenticatorAssertionResponse struct {
	ClientDataJSON    []int `json:"clientDataJSON"`
	AuthenticatorData []int `json:"authenticatorData"`
	Signature         []int `json:"signature"`
}

// CollectedClientData represents the contextual bindings of both the WebAuthn
// Relying Party and the client.
// Challenge is a byte array so that json marshal automatically parforms base64
// encoding.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-client-data
type CollectedClientData struct {
	Type        string `json:"type"`
	Challenge   string `json:"challenge"`
	Origin      string `json:"origin"`
	CrossOrigin bool   `json:"crossOrigin"`
}

// GetClientDataJSON makes AuthenticatorAttestationResponse implement the
// AuthenticatorResponse interface.
func (r *AuthenticatorAttestationResponse) GetClientDataJSON() []int {
	return r.ClientDataJSON
}

// GetClientDataJSON makes AuthenticatorAssertionResponse implement the
// AuthenticatorResponse interface.
func (r *AuthenticatorAssertionResponse) GetClientDataJSON() []int {
	return r.ClientDataJSON
}

// ChallengeResponse represents the authenticator's request for the client to
// sign the given challenge using its member private key.
type ChallengeResponse struct {
	Session   string `json:"session"`
	Challenge string `json:"challenge"`
}
