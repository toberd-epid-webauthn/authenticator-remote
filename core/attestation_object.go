// Copyright 2022 Tobias Berdin

package core

import (
	"authenticator-remote/util"
	"github.com/fxamacker/cbor/v2"
)

// AttestationObject is a CBOR map containing three fields for device
// attestation.
type AttestationObject struct {
	Fmt      string          `json:"fmt"`
	AuthData []byte          `json:"authData"`
	AttStmt  cbor.RawMessage `json:"attStmt,omitempty"`
}

// CredentialPublicKeyES256 is the COSE representation of an ES256 public key.
//
// see https://datatracker.ietf.org/doc/html/rfc8152#section-13.1
type CredentialPublicKeyES256 struct {
	Kty uint8  `cbor:"1,keyasint"`
	Alg int8   `cbor:"3,keyasint"`
	Crv uint8  `cbor:"-1,keyasint,omitempty"`
	X   []byte `cbor:"-2,keyasint,omitempty"`
	Y   []byte `cbor:"-3,keyasint,omitempty"`
}

// AuthenticatorData structure encodes contextual bindings made by the
// authenticator.
//
// see https://www.w3.org/TR/webauthn-3/#sctn-authenticator-data
type AuthenticatorData struct {
	RpIDHash               []byte
	Flags                  byte
	SignCount              uint32
	AttestedCredentialData AttestedCredentialData
}

// AttestedCredentialData is a variable-length byte array added to the
// authenticator data when generating an attestation object for a given
// credential.
//
// see https://www.w3.org/TR/webauthn-3/#sctn-attested-credential-data
type AttestedCredentialData struct {
	CredentialIDLength  uint16
	CredentialID        string
	CredentialPublicKey []byte
}

// ToByteArray returns the byte representation of the given AuthenticatorData
// needed in the AttestationObject.
func (a *AuthenticatorData) ToByteArray() []byte {
	ret := a.RpIDHash
	// append flags
	ret = append(ret, a.Flags)
	// append counter
	ret = append(ret, util.Uint32ToByte(a.SignCount)...)
	// append attested credential data
	ret = append(ret, a.AttestedCredentialData.toByteArray()...)
	return ret
}

// ToByteArray returns the byte representation of the given
// AttestedCredentialData needed in the AttestationObject.
func (a *AttestedCredentialData) toByteArray() []byte {
	ret := aaguid
	// append length of credential id
	ret = append(ret, util.Uint16ToByte(a.CredentialIDLength)...)
	// append credential id
	ret = append(ret, []byte(a.CredentialID)...)
	// append credential public key
	ret = append(ret, a.CredentialPublicKey...)
	return ret
}
