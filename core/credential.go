// Copyright 2021 Tobias Berdin

package core

// PublicKeyCredentialCreationOptions are options for credential creation.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-makecredentialoptions
type PublicKeyCredentialCreationOptions struct {
	Rp   PublicKeyCredentialRpEntity   `json:"rp"`
	User PublicKeyCredentialUserEntity `json:"user"`

	Challenge        []byte                          `json:"challenge"`
	PubKeyCredParams []PublicKeyCredentialParameters `json:"pubKeyCredParams"`
}

// PublicKeyCredentialRpEntity is used to supply additional Relying Party
// attributes when creating a new credential.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-rp-credential-params
type PublicKeyCredentialRpEntity struct {
	Name string `json:"name"`
	Id   string `json:"id"`
}

// PublicKeyCredentialUserEntity is used to supply additional user account
// attributes when creating a new credential.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-user-credential-params
type PublicKeyCredentialUserEntity struct {
	Name        string `json:"name"`
	Id          []byte `json:"id"`
	DisplayName string `json:"displayName"`
}

// PublicKeyCredentialParameters is used to supply additional parameters when
// creating a new credential.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-credential-params
type PublicKeyCredentialParameters struct {
	ParamType string `json:"type"`
	Alg       int16  `json:"alg"`
}

// PublicKeyCredentialRequestOptions are options for credential request
// upon login.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-assertion-options
type PublicKeyCredentialRequestOptions struct {
	RpId             string                          `json:"rpId"`
	Challenge        []byte                          `json:"challenge"`
	AllowCredentials []PublicKeyCredentialDescriptor `json:"allowCredentials"`
}

// PublicKeyCredentialDescriptor contains the attributes that are specified by
// a caller when referring to a public key credential.
//
// see https://www.w3.org/TR/webauthn-3/#dictionary-credential-descriptor
type PublicKeyCredentialDescriptor struct {
	Type string `json:"type"`
	Id   []byte `json:"id"`
}
