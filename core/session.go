// Copyright 2022 Tobias Berdin

package core

// sessions maps a session to the challenge that was created with it.
var sessions map[string]string

// InitSessions initializes the sessions map.
func InitSessions() {
	sessions = make(map[string]string)
}

// AddSession creates a new session entry for the id.
func AddSession(id string, entry string) {
	sessions[id] = entry
}

// GetSession removes and returns the session entry for the id. Returns nil if
// entry doesn't exist.
func GetSession(id string) *string {
	if entry, ok := sessions[id]; ok {
		delete(sessions, id)
		return &entry
	}
	return nil
}
